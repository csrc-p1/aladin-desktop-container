FROM dorowu/ubuntu-desktop-lxde-vnc:latest
RUN \
#### Dependencies
apt update ||true && \
apt-get install nvidia-cuda-toolkit -y 
RUN \
apt install -y openjdk-11-jdk && \
apt install -y icedtea-netx && \
useradd --shell=/bin/bash --home /home/pub-user pub-user && \
echo 'pub-user:password' | chpasswd && \
#### Install Aladin desktop 
wget -P /home/pub-user/ https://aladin.cds.unistra.fr/java/Aladin.jar && \
wget -P /home/pub-user/ https://aladin.cds.unistra.fr/java/Aladin && \
chmod 777 /home/pub-user/Aladin && \
wget -P /home/pub-user/ https://aladin.cds.unistra.fr/java/AladinBeta.jar && \
wget -P /home/pub-user/ https://aladin.cds.unistra.fr/java/AladinBeta.jnlp && \
chmod 777 /home/pub-user/AladinBeta.jnlp
