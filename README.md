# Aladin desktop container

This container is for the Aladin desktop deployment, using a web VNC interface to access the Ubuntu desktop environment.

This container is base on the docker-ubuntu-vnc-desktop project, you can find in [GitHub page](https://github.com/fcwu/docker-ubuntu-vnc-desktop) and [Docker hub](https://hub.docker.com/r/dorowu/ubuntu-desktop-lxde-vnc).

We used the instructions and examples provided on the [AladinLite](https://aladin.u-strasbg.fr/AladinLite/doc/), and [Aladin Desktop](https://aladin.cds.unistra.fr/java/nph-aladin.pl?frame=downloading)

## Pull IMAGE

If you don't want to make any changes, just use this command to pull the IMAGE from Docker Hub:
```
docker pull cskap/aladin_desktop_x11vnc:v2
```

## Build IMAGE

If you want to build it yourself, follow these steps:

1) Download the Dockerfile.

2) Build IMAGE:
    ```
    docker build -t [your docker id]/[IMAGE name]:[tag] .
    ```

## Run Container
```
docker run -itd -p 443:443 -v /home:/carta/users --name mycarta cskap/ubuntu_carta:v2 /bin/bash
```
This will run the container in the background, and you can now use a web browser to access in "https://localhost:6080(or the IP address your container running with the port you set)".

If you build the IMAGE with another name, just change "cskap/aladin_desktop_x11vnc:v2" to your IMAGE name.

## Add Data Volumes
You can easily add data volumes with multiple "-v /[host folder]:/[container folder]". for example:
```
-v /home/pub:/home/pub
```

## TODO
- [ ] Add cuda GPU support.

- [ ] Change root to non-root user with the local user uid.

- [ ] Add Aladin Lite in Jupyter Hub
